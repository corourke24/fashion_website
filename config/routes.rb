Rails.application.routes.draw do

  root 'static_pages#home'
  get '/catalogue',        to: 'full_catalogue#catalogue'
  get '/contact',          to: 'messages#new', as: 'new_message'
  post '/contact',         to: 'messages#create', as: 'create_message'
  
end
