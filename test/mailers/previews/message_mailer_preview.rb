# Preview all emails at http://localhost:8080/rails/mailers/message_mailer
class MessageMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:8080/rails/mailers/message_mailer/contact_me
  def contact_me
    message = Message.new name: 'Christian', 
                          email: 'Christian@example.org',
                          content: 'Email content'
    MessageMailer.contact_me message
  end

end
