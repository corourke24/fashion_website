require 'test_helper'

class MessageMailerTest < ActionMailer::TestCase
  test "contact_me" do
    message = Message.new name: 'Christian',
                          email: 'Christian@example.org',
                          content: 'hello!'

    email = MessageMailer.contact_me(message)

    assert_emails 1 do
      email.deliver_now
    end

    assert_equal "Message from Christian O'Rourke Fashion", email.subject
    assert_equal ['christian.c.orourke@gmail.com'], email.to
    assert_equal ['Christian@example.org'], email.from
    assert_match /hello!/, email.body.encoded
  end

end
