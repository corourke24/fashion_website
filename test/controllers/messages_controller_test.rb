require 'test_helper'

class MessagesControllerTest < ActionDispatch::IntegrationTest
  
  test "GET new" do
    get new_message_url
    assert_response :success
    assert_select 'form' do 
      assert_select 'input[type=text]'
      assert_select 'input[type=email]'
      assert_select 'textarea'
      assert_select 'input[type=submit]'
    end
  end
  
  test "create message" do
    assert_difference 'ActionMailer::Base.deliveries.size', 1 do
      post create_message_url, params: {
        message: {
          name: 'Christian',
          email: 'Christian@example.org',
          content: 'test'
        }
      }
    end
    assert_redirected_to root_path
    follow_redirect!
    assert_match "Thank you for your message!", response.body
  end
  
  test "blank messages should be invalid" do
    post create_message_url, params: {
      message: { name: '', email: '', content: '' }
    }
    assert_match /Name .* blank/, response.body
    assert_match /Email .* blank/, response.body
    assert_match /Content .* blank/, response.body
  end
end
