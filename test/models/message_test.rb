require 'test_helper'

class MessageTest < ActiveSupport::TestCase

  test 'responds to name, email and body' do 
    msg = Message.new
    assert msg.respond_to?(:name),  'does not respond to :name'
    assert msg.respond_to?(:email), 'does not respond to :email'
    assert msg.respond_to?(:content),  'does not respond to :content'
  end
  
  test 'should be valid when all attributes are set' do
    attributes = { 
      name: 'Christian',
      email: 'christian@example.org',
      content: 'example message'
    }
    msg = Message.new attributes
    assert msg.valid?, 'should be valid'
  end
  
  test 'name, email and body should be present' do
    msg = Message.new

    refute msg.valid?, 'Blank Mesage should be invalid'

    assert_match "", msg.errors[:name].to_s
    assert_match "", msg.errors[:email].to_s
    assert_match "", msg.errors[:body].to_s
  end
  
end
